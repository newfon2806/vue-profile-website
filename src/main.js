import Vue from 'vue';
import App from './App.vue';
import router from './router';
import BootstrapVue from 'bootstrap-vue';
import VueTimeline from "@growthbunker/vuetimeline";
import 'fullpage.js/vendors/scrolloverflow'
import VueFullPage from 'vue-fullpage.js';
import Highcharts from 'highcharts'
import VueHighcharts from 'vue-highcharts'
import highchartsMore from 'highcharts/highcharts-more'

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import './assets/icon-font/fontello.css';
import 'fullpage.js/dist/fullpage.min.css';

Vue.use(BootstrapVue);
Vue.use(VueFullPage);
Vue.use(VueTimeline);
highchartsMore(Highcharts)
Vue.use(VueHighcharts, {Highcharts})

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
