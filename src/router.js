import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const router = new VueRouter({
  linkExactActiveClass: 'active',
  routes: [
    {
      path: '/',
      name: 'suppawits',
      component: () => import('./Pages/suppawits.vue'),
    }
  ]
})

export default router;